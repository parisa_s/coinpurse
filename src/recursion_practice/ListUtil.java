package recursion_practice;

import java.util.Arrays;
import java.util.List;

public class ListUtil {
		
		private static void printList(List<String> list) {
			if(list.size()==0) return ;

				printList(list.subList(0, list.size()-1));
				System.out.print(list.get(list.size()-1)+" ,");
			
		}
		
		private static String max(List<String> list) {
			if(list.size()==1) 
				return list.get(0) ;

			String left = list.get(0);
			String right = max(list.subList(1,list.size()));
			if(left.compareTo(right)>0) {
				return left;
			}
			else {
				return right;
			}

		}
		public static void main(String [] args) {
			List<String> list;
			
			if(args.length > 0) {
				list = Arrays.asList(args);
			}
			else {
				list = Arrays.asList("bird","zebra","cat","pig","peak");
			}
			
			System.out.print("List contains: ");
			printList(list);
			
			String max = max(list);
			System.out.println("Lexically greatest element is "+max);
		}
}
