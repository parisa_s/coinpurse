package coinpurse;
/**
 * Abstract class of valuable interface.
 * @author Parisa Supitayakul
 */
public abstract class AbstractValuable implements Valuable{
	
	protected double value;
	/** 
     * Constructor for an abstracValuable. 
     */
	public AbstractValuable() {
		super();
	}
	/** 
     * Compare value of money with other kind money.
     * @param obj is the money that use for compare. 
     * @return -1 when a value of coin is less than other.
     * 		   0  when a value of coin is equals to other.
     * 		    1 when a value of coin is more than other.
     */
    public int compareTo(Valuable obj) {
    	if(this.getValue() < obj.getValue()) {
    		return -1;
    	}
    	else if(this.getValue() == obj.getValue()) {
    		return 0;
    	}
    	else {
    		return 1;
    	}
    }
	/** 
	 * To check the value of money that equals with other.
	 * @param obj is the object that use for compare.
	 * @return true if a value of money is equals to other.   
	 */
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		if(obj.getClass() != this.getClass()) {
			return false;
		}
		Valuable object = (Valuable) obj;
		if(object.getValue() == this.getValue()) {
			return true;
		}
	
		return false;
		
	}
	/** 
     * Get the value of money. 
     * @return value of money.
     */
	public double getValue() {
		return value;
	}

}