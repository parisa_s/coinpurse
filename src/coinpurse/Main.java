package coinpurse;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Parisa Supitayakul
 */
public class Main {

    /**
     * @param args not used
     */
    public static void main( String[] args ) {
    	Purse purse = new Purse(15);
    	purse.setWithdrawStrategy(new RecursiveWithdraw());
    	ConsoleDialog console = new ConsoleDialog(purse);
    	purse.addObserver(new PurseObserver());
    	
    	PurseBalanceObserver balanceObserver = new PurseBalanceObserver();
    	PurseStatusObserver statusObserver = new PurseStatusObserver();
    	
    	purse.addObserver(balanceObserver);
    	purse.addObserver(statusObserver);
    	
    	balanceObserver.run();
    	statusObserver.run();
    	
    	console.run();
    }
}
