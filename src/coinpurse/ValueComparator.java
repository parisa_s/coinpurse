package coinpurse;

import java.util.Comparator;

/**
 * A class that use for compare two valuable object.
 * @author Parisa Supitayakul
 */
public class ValueComparator implements Comparator<Valuable>{
	
	/** 
     * Compare value of object with other object.
     * @param  a is the first object that use for compare. 
     * @param  b is other object that use for compare with the first object.
     * @return -1 when a value of first object is less than other.
     * 		    0  when a value of first object is equals to other.
     * 		    1 when a value of first object is more than other.
     */
	public int compare(Valuable a,Valuable b) {
		if(a.getValue()<b.getValue()) {
			return -1;
		}
		if(a==b) {
			return 0;
		}
		else {
			return 1;
		}
	}

}
