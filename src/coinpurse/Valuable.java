package coinpurse;

/**
 * This interface use for get the value of object.
 * @author Parisa Supitayakul
 */
public interface Valuable extends Comparable<Valuable>{

	/**
	 * Get the value of object.
     *  @return the value of object.
	 */
	public double getValue( );
}
