package coinpurse;
 

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.WithdrawStrategy;

/**
 *  A coin purse contains money.
 *  You can insert money, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  money to remove.
 *  
 *  @author Parisa Supitayakul
 */
public class Purse extends Observable{
    /** Collection of money in the purse. */

    
    /** Capacity is maximum NUMBER of money the purse can hold.
     *  Capacity is set when the purse is created.
     */
    private ArrayList<Valuable> moneyList;
    private int capacity;
    private WithdrawStrategy strategy;
    
    /** 
     *  Create a purse with a specified capacity.
     *  @param capacity is maximum number of money you can put in purse.
     */
    public Purse( int capacity ) {

    	this.capacity = capacity;
    	moneyList = new ArrayList<Valuable>();
    }

    /**
     * Count and return the number of money in the purse.
     * This is the number of money, not their value.
     * @return the number of money in the purse
     */
    public int count() { 
    	return moneyList.size(); 
    }
    
    /** 
     *  Get the total value of all items in the purse.
     *  @return the total value of items in the purse.
     */
    public int getBalance() {
    	double total=0;
    	for(int i=0;i<moneyList.size();i++) {
    		total += moneyList.get(i).getValue();
    	}
    	return (int)total; 
    }

    
    /**
     * Return the capacity of the coin purse.
     * @return the capacity
     */

    public int getCapacity() { 
    	return capacity; 
    }
    
    /** 
     *  Test whether the purse is full.
     *  The purse is full if number of items in purse equals
     *  or greater than the purse capacity.
     *  @return true if purse is full.
     */
    public boolean isFull() {
    	if(this.getCapacity()==this.count()) {
    		return true;
    	}
    	
    	return false;
    	

    }

    /** 
     * Insert a valuable object into the purse.
     * The object is only inserted if the purse has space for it
     * and the object has positive value.  No worthless money!
     * @param obj is an object to insert into purse
     * @return true if obj inserted, false if can't insert
     */
    public boolean insert(Valuable obj ) {
        // if the purse is already full then can't insert anything.
    	if(this.isFull()||obj.getValue() <= 0) {
    		return false;
    	}
    	else {
    		moneyList.add(obj);
    		super.setChanged();
    		super.notifyObservers();
    		return true;
    	}
    }
    
    /**  
     *  Withdraw the requested amount of money.
     *  Return an array of Coins withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @return array of Coin objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
    public Valuable[] withdraw(double amount){
    	ArrayList<Valuable> newPurse = strategy.withdraw(amount, moneyList);
    	if(newPurse==null) {
    		return null;
    	}
    	// success! remove  the money from the purse
    	for (int j = 0; j < newPurse.size(); j++) {
    		Valuable money = newPurse.get(j);
    		// remove from purse
    		moneyList.remove(money);
    				
    	}
    			
    	Valuable [] newMoney = new Valuable[newPurse.size()];
    	newPurse.toArray(newMoney);
    	
    	super.setChanged();
    	super.notifyObservers();
    	
    	return newMoney;
	}
  
    /**
     * To set the strategy of withdraw.
     * @param withdrawStategy is the strategy algorithm of withdraw. 
     */
    public void setWithdrawStrategy(WithdrawStrategy  withdrawStrategy) { 
    	this.strategy = withdrawStrategy;
    }
    /** 
     * toString returns a string description of the purse contents.
     * It can return whatever is a useful description.
     * @return String of coin and value of coin.
     */
    public String toString() {
    	String toString = "";
    	for(int i=0;i<moneyList.size();i++) {
    		toString += String.format("%s\n", moneyList.get(i).toString());
    	}
    	return toString;
    }

}
