package coinpurse.strategy;

import java.util.ArrayList;
import java.util.List;

import coinpurse.Valuable;

/**
 * A class  WithdrawStrategy class to use for withdraw the money by recursion.
 * @author Parisa Supitayakul
 */
public class RecursiveWithdraw implements WithdrawStrategy{
	
	/**  
     *  Withdraw the requested amount of money by recursion.
     *  if withdraw successful it will return the list of money that user request
     *  or return null if it unsuccessful.
     *  @param amount is a total money that used for withdraw.
     *  	   valuable is a list of money in purse.
     *  @return list of money for withdraw or null if it cannot withdraw.
     */
	public ArrayList<Valuable> withdraw(double amount,List<Valuable> valuable){
		int lastindex = valuable.size()-1;
		return withdrawForm(amount,valuable,lastindex);
	}
	/**  
     *  A method for help withdraw method about recursion.
     *  @param amount is a total money that used for withdraw.
     *  	   valuable is a list of money in purse.
     *         lastindex is a size of valuable list.
     *  @return list of money for withdraw or null if it cannot withdraw.
     */
	public ArrayList<Valuable> withdrawForm(double amount,List<Valuable> valuable,int lastindex) {
		lastindex = valuable.size()-1;
		if(amount<=0 || lastindex < 0 || valuable.size()==0 ) 
			return null;
		
		if( valuable.get(lastindex).getValue() < amount) {
			List<Valuable> purseList = withdrawForm(amount-valuable.get(lastindex).getValue(),valuable.subList(0, lastindex),lastindex-1);
			if(purseList != null) {
				purseList.add(valuable.get(lastindex));
				return (ArrayList<Valuable>) purseList;
			}
			withdrawForm(amount,valuable.subList(0, lastindex),lastindex-1);
		}
		else if(valuable.get(lastindex).getValue() == amount){
			ArrayList<Valuable> purse = new ArrayList<Valuable>();
			purse.add(valuable.get(lastindex));
			return purse;
		}
		
		return withdrawForm(amount,valuable.subList(0, lastindex),lastindex-1);
	}
	
}
