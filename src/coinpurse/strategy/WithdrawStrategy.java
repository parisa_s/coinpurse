package coinpurse.strategy;

import java.util.ArrayList;
import java.util.List;

import coinpurse.Valuable;

/**
 * This interface that use for implements other type of WithdrawStrategy.
 * @author Parisa Supitayakul
 */
public interface WithdrawStrategy{
	/** 
     * Withdraw method that used for remove the money from the purse by amount. 
     * @param amount is the value of money that used to withdraw.
     * 		  valuable is a list of money in purse.
     * @return the list of money that user withdraw.  
     */
	public ArrayList<Valuable> withdraw(double amount,List<Valuable> valuable);
}
