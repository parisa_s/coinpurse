package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
import coinpurse.ValueComparator;
/**
 * A class is WithdrawStrategy class to use for withdraw the money by greedy technique.
 * @author Parisa Supitayakul
 */
public class GreedyWithdraw implements WithdrawStrategy{
	
	
	/**  
     *  Withdraw the requested amount of money.
     *  Return an array of Coins withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @return array of Coin objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
	public ArrayList<Valuable> withdraw(double amount,List<Valuable> valuable){

    	if ( amount <= 0 ) {	
			return null;
    	}
	
		ArrayList<Valuable> purse2 = new ArrayList<Valuable>();
		Collections.sort(valuable,new ValueComparator());
		Collections.reverse(valuable);
		
		// try to find the amount we need and record
		// which money are used by copying the references

		for (int i = 0; i < valuable.size() ; i++) {
			if (valuable.get(i).getValue() <= amount) {
				// use this coin!
				amount -= valuable.get(i).getValue();
				purse2.add(valuable.get(i));
			}
		}
		// if we didn't get the full amount, then return failure
				if (amount > 0) {
					return null;
				}
	
		
        return purse2;
		

	}
}
