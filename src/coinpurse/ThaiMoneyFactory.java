package coinpurse;


/**
 * Factory for create Thai money including baht coin and banknote.
 * @author Parisa Supitayakul
 *
 */
public class ThaiMoneyFactory extends MoneyFactory{
	
	/**
	 * Constructor for factory.
	 */
	ThaiMoneyFactory() {		
	}
	
	/**
	 * To create money that have value given in parameter.
	 * @param value is the value that used to create a money.
	 * @return valuable of money that created.
	 */
	public Valuable createMoney(double value){

		if(value == 1 ||value == 2 ||value == 5 ||value == 10 ){
			return  new Coin(value,"baht");
		}
		else if(value == 20 ||value == 50 ||value == 100 ||value == 500 || value ==1000){
			return new BankNote(value,"baht");
		}
		
		throw new IllegalArgumentException();
	}

	
}
