package coinpurse;
 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Parisa Supitayakul
 */
public class Coin extends AbstractValuable {
	private String currency = null;

    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value ,String currency) {
        this.value = value;
        this.currency = currency;
    }
 
    /** 
     * Returns a string description of the coin.
     * @return String of the value of coin.
     */
    public String toString() {
    	return (int)value +" "+ currency +" coin";
    }
    
}

