package coinpurse;

/**
 * A banknote with value and serial number.
 * @author Parisa Supitayakul
 */
public class BankNote extends AbstractValuable {
	
	private int serialNumber;
	private static int nextSerialNumber = 1000000;
	private String currency = null;
	
	/** 
     * Constructor for a new banknote. 
     * @param value is a value of banknote.
     */
	public BankNote(double value , String currency) {
		this.serialNumber = this.getNextSerialNumber();
		this.value = value;
		this.currency = currency;
	}
	
	/** 
     * Get the serialNumber of banknote. 
     * @return serialNumber of banknote.
     */
	public static int getNextSerialNumber() {
		return nextSerialNumber++;
	}
	/** 
     * Returns a string description of the banknote.
     * @return String of the value of banknote.
     */
	public String toString() {
		return String.format("%.2f-%s Banknote [%d]", this.value,this.currency,this.serialNumber);
	}
}
