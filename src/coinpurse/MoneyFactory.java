package coinpurse;

import java.util.ResourceBundle;

/**
 * Factory for create money that depends on local currency.
 * @author Parisa Supitayakul
 *
 */
public abstract class MoneyFactory {
	/** create moneyFactory*/
	private static MoneyFactory moneyFactory = null;
	
	/**
	 * Constructor for Factory.
	 */
	MoneyFactory(){
		
	}
	
	/**
	 * Get the moneyFactory object depends on type factory 
	 * from purse.properties and create a factory if it's null.
	 * @return moneyFactory that have same type as purse.properties.
	 */
	public static MoneyFactory getInstance(){
		ResourceBundle bundle = ResourceBundle.getBundle("purse");
		String factoryclass = bundle.getString("moneyfactory");
		if(moneyFactory == null){
			try {
				moneyFactory = (MoneyFactory)Class.forName(factoryclass).newInstance();
			} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return moneyFactory;
	}
	
	/**
	 * To create money that have value given in parameter.
	 * @param value is the value that used to create a money.
	 * @return valuable of money that created.
	 */
	public abstract Valuable createMoney(double value);
	
	
	/**
	 * To convert money from string to double 
	 * if it can convert ,it will create money. 
	 * @param value is the value that used to convert from string to double.
	 * @return valuable of money that created.
	 */
	public Valuable createMoney(String value){
		Valuable moneyValue = null ;
		
		try{
			Double money = Double.parseDouble(value);
			moneyValue = createMoney(money);
		}
		catch(NumberFormatException e ){
			e.getMessage();
		}
		return moneyValue;
	}
	
	/**
	 * To set MoneyFactory.
	 * @param moneyFact is a factory that used for set.
	 */
	public static void setMoneyFactory(MoneyFactory moneyFact){
		moneyFactory  = moneyFact;
	}
	
	/**
	 * To test class
	 * @param args 
	 */
	public static void main(String [] args){

		 MoneyFactory factory = MoneyFactory.getInstance();
		 
		 Valuable m = factory.createMoney( 5 );
		 System.out.println(m.toString());
		 Valuable m2 = factory.createMoney("0.05");
		 System.out.println(m2.toString());


	}
}
