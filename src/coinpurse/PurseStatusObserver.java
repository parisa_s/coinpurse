package coinpurse;

import java.awt.Font;
import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

/**A graphic user interface that observe and update the status of the purse.
 * @author Parisa Supitayakul
 */
public class PurseStatusObserver extends JFrame implements Observer{
	JLabel statusLabel ;
	JProgressBar statusBar;
	
	/** 
     * Constructor for create purseStatusObserver. 
     */
	public PurseStatusObserver() {
		this.initComponent();
	}
	
	/** 
     *Create the graphic user interface. 
     */
	private void initComponent() {
		setLayout(new GridLayout(2,1));
		setTitle("Purse Status Observer");
		
		statusLabel = new JLabel("EMPTY");
		statusBar = new JProgressBar();
		statusLabel.setFont(new Font("Arial",Font.CENTER_BASELINE,25));
		super.add(statusLabel);
		super.add(statusBar);
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	/** 
     *Run the graphic use interface. 
     */
	public void run() {
		this.pack();
		this.setVisible(true);
	}
	
	/** 
     *Update status and bar to show percent of purse  by  graphic user interface. 
     */
	public void update(Observable subject,Object info) {
		if(subject instanceof Purse) {
			Purse purse = (Purse)subject;
			
			statusBar.setMaximum(purse.getCapacity());
			statusBar.setValue(purse.count());
			
			if(purse.count()==0) {
				statusLabel.setText("EMPTY");
			}
			else if(purse.isFull()) {
				statusLabel.setText("FULL");
			}
			else {
				statusLabel.setText(purse.count()+ " items");
			}
			
		}
		if(info != null) {
			System.out.println(info);
		}
	}
}
