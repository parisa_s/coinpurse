package coinpurse;

import java.util.Observable;
import java.util.Observer;

/**A class that observe the purse.
 * @author Parisa Supitayakul
 */
public class PurseObserver implements Observer{
	
	/** 
     * Constructor for purseObserver. 
     */
	public PurseObserver() {
		
	}
	
	/** 
     *Update balance to  graphic user interface. 
     */
	public void update(Observable subject,Object info) {
		if(subject instanceof Purse) {
			Purse purse = (Purse)subject;
			int balance = purse.getBalance();
			System.out.println("Balance is: "+ balance);
		}
		if(info != null) {
			System.out.println(info);
		}
	}
	
	
}
