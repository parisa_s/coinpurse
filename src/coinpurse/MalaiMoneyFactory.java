package coinpurse;

/**
 * Factory for create malaysia money including sen and ringgit.
 * @author Parisa Supitayakul
 *
 */
public class MalaiMoneyFactory extends MoneyFactory{

	/**
	 * Constructor for factory.
	 */
	MalaiMoneyFactory() {
	}
	
	/**
	 * To create money that have value given in parameter.
	 * @param value is the value that used to create a money.
	 * @return valuable of money that created.
	 */
	public Valuable createMoney(double value) {

		if(value == 0.05 ||value == 0.10 ||value == 0.20 ||value == 0.50 ){
			return new Coin(value*100,"sen");
		}
		else if(value == 1 ||value == 2 ||value == 5 ||value == 10 ||value == 20 ||value == 50 ||value == 100 ){
			return new BankNote(value,"ringgit");
		}
		throw new IllegalArgumentException();
	}

}
