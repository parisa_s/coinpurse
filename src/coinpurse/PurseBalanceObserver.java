package coinpurse;

import java.awt.Font;
import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**A graphic user interface that observe and update the balance in the purse.
 * @author Parisa Supitayakul
 */
public class PurseBalanceObserver extends JFrame implements Observer{
	private JLabel balanceLabel;
	
	/** 
     * Constructor for create purseBalanceObserver. 
     */
	public PurseBalanceObserver() {
		this.initComponent();
	}
	
	/** 
     *Create the graphic user interface. 
     */
	private void initComponent() {
		setLayout(new GridLayout(1,1));
		setTitle("Purse Balance Observer");
		
		balanceLabel = new JLabel();
		balanceLabel.setFont(new Font("Arial",Font.CENTER_BASELINE,25));
		balanceLabel.setText("0 Baht");
		
		super.add(balanceLabel);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	/** 
     *Run the graphic use interface. 
     */
	public void run() {
		this.pack();
		this.setVisible(true);
	}
	
	/** 
     *Update balance to  graphic user interface. 
     */
	public void update(Observable subject,Object info) {
		if(subject instanceof Purse) {
			Purse purse = (Purse)subject;
			int balance = purse.getBalance();
			//System.out.println("!!!!Balance is: "+ balance);
			balanceLabel.setText(balance + " Baht");
		}
		if(info != null) {
			System.out.println(info);
		}
	}
}
