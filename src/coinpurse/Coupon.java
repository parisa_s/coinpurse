package coinpurse;

import java.util.HashMap;
import java.util.Map;

/**
 * A Coupon with value and color of coupon.
 * @author Parisa Supitayakul
 */
public class Coupon extends AbstractValuable{
	/** list of the coupon. */
	private Map<String,Double> colorCoupon = new HashMap<String,Double>();
	/** Color of the coupon. */
	private String color;
	/** 
     * Constructor for a new coupon. 
     * @param color is a color of coupon.
     */
	public Coupon(String color) {
		colorCoupon.put("red", 100.0 );
		colorCoupon.put("blue", 50.0);
		colorCoupon.put("green", 20.0);
		
		this.color = color.toLowerCase();
		if(!colorCoupon.containsKey(this.color)){
			this.color = null;
		}
	}
	/** 
     * Get the value of coupon. 
     * @return value of coupon.
     */
	public double getValue() {
		return colorCoupon.get(color);
	}
	/** 
     * Returns a string description of the coupon.
     * @return String of the value of coupon.
     */
	public String toString() {
		return String.format("%s Coupon", color);
	}
}
